# CoolDash – Blank Magento extension for building admin system configuration area • Inchoo

By: Branko Ajzele, Aug 21, 2009 

![][1]

In order to speed things up with building admin sections under System > Configuration area in Magento I wrote a little blank extension. Hopefully its a step or two beyond "Hello world" level. I named the extension "CoolDash", short from CoolDashboard. Name holds no special meaning, just something I popped out of my head. First thing you might notice when you open config.xmls and system.xml is the attribute naming. I intentionally used names like "somecooldashmodel2". I found it extremely difficult, error prone and annoying to get around in scenarios where different areas of config files use same names for attributes with different meaning, like "customer", "catalog" and so on.

Hopefully my "funny" naming scheme in config files will give you a better overview on where everything goes. Below are few screenshots to see the result.

![SystemConfAdminArea][2]

[Download ActiveCodeline_CoolDash][3]._Blank extension for building "System > Configuration" area of Magento admin interface_. Although built on Magento 1.4alpha, should work on Magento 1.3.

Hope it helps.

[1]: https://gitlab.com/magento1x/inchoo_cooldash/raw/master/cooldash.jpg "CoolDash – Blank Magento extension for building admin system configuration area"
[2]: https://gitlab.com/magento1x/inchoo_cooldash/raw/master/SystemConfAdminArea.png "SystemConfAdminArea"
[3]: https://gitlab.com/magento1x/inchoo_cooldash/raw/master/ActiveCodeline_CoolDash.zip